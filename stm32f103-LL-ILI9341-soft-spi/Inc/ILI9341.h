#ifndef ILI9341_H
#define ILI9341_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "stm32f1xx_ll_gpio.h"
#include "fonts.h"

/*
#define ILI9341_NOP       0x00
#define ILI9341_SWRESET   0x01
#define ILI9341_RDDID     0x04
#define ILI9341_RDDST     0x09

#define ILI9341_SLPIN     0x10
#define ILI9341_SLPOUT    0x11
#define ILI9341_PTLON     0x12
#define ILI9341_NORON     0x13

#define ILI9341_RDMODE    0x0A
#define ILI9341_RDMADCTL  0x0B
#define ILI9341_RDPIXFMT  0x0C
#define ILI9341_RDIMGFMT  0x0D
#define ILI9341_RDSELFDIAG 0x0F

#define ILI9341_INVOFF    0x20
#define ILI9341_INVON     0x21
#define ILI9341_GAMMASET  0x26
#define ILI9341_DISPOFF   0x28
#define ILI9341_DISPON    0x29

#define ILI9341_CASET     0x2A
#define ILI9341_PASET     0x2B
#define ILI9341_RAMWR     0x2C
#define ILI9341_RAMRD     0x2E

#define ILI9341_PTLAR     0x30
#define ILI9341_MADCTL    0x36
#define ILI9341_PIXFMT    0x3A

#define ILI9341_FRMCTR1   0xB1
#define ILI9341_FRMCTR2   0xB2
#define ILI9341_FRMCTR3   0xB3
#define ILI9341_INVCTR    0xB4
#define ILI9341_DFUNCTR   0xB6

#define ILI9341_PWCTR1    0xC0
#define ILI9341_PWCTR2    0xC1
#define ILI9341_PWCTR3    0xC2
#define ILI9341_PWCTR4    0xC3
#define ILI9341_PWCTR5    0xC4
#define ILI9341_VMCTR1    0xC5
#define ILI9341_VMCTR2    0xC7

#define ILI9341_RDID1     0xDA
#define ILI9341_RDID2     0xDB
#define ILI9341_RDID3     0xDC
#define ILI9341_RDID4     0xDD

#define ILI9341_GMCTRP1   0xE0
#define ILI9341_GMCTRN1   0xE1
*/

/*
#define ILI9341_PWCTR6    0xFC
*/

// Color definitions
#define ILI9341_BLACK       0x0000      /*   0,   0,   0 */
#define ILI9341_NAVY        0x000F      /*   0,   0, 128 */
#define ILI9341_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define ILI9341_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define ILI9341_MAROON      0x7800      /* 128,   0,   0 */
#define ILI9341_PURPLE      0x780F      /* 128,   0, 128 */
#define ILI9341_OLIVE       0x7BE0      /* 128, 128,   0 */
#define ILI9341_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define ILI9341_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define ILI9341_BLUE        0x001F      /*   0,   0, 255 */
#define ILI9341_GREEN       0x07E0      /*   0, 255,   0 */
#define ILI9341_CYAN        0x07FF      /*   0, 255, 255 */
#define ILI9341_RED         0xF800      /* 255,   0,   0 */
#define ILI9341_MAGENTA     0xF81F      /* 255,   0, 255 */
#define ILI9341_YELLOW      0xFFE0      /* 255, 255,   0 */
#define ILI9341_WHITE       0xFFFF      /* 255, 255, 255 */
#define ILI9341_ORANGE      0xFD20      /* 255, 165,   0 */
#define ILI9341_GREENYELLOW 0xAFE5      /* 173, 255,  47 */
#define ILI9341_PINK        0xF81F

// Memory Access Control
#define ILI9341_MADCTL     0x36
#define ILI9341_MADCTL_MY  0x80  // < Bottom to top
#define ILI9341_MADCTL_MX  0x40  // < Right to left
#define ILI9341_MADCTL_MV  0x20  // < Reverse Mode
#define ILI9341_MADCTL_ML  0x10  // < LCD refresh Bottom to top
#define ILI9341_MADCTL_RGB 0x00  // < Red-Green-Blue pixel order
#define ILI9341_MADCTL_BGR 0x08  // < Blue-Green-Red pixel order
#define ILI9341_MADCTL_MH  0x04  // < LCD refresh right to left
#define ILI9341_MADCTL_SWAP_XY_Mirror_Y  0xa0  // < X-Y Exchange,Y-Mirror
#define ILI9341_MADCTL_SWAP_XY_Mirror_X  0x60  // < X-Y Exchange,X-Mirror
#define ILI9341_MADCTL_Mirror_XY  0xc0         // < X-Mirror,Y-Mirror: Bottom to top; Right to left; RGB
#define ILI9341_ROTATION    (ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR)

#define swap(a,b) {int16_t t=a;a=b;b=t;}

void ILI9341_SetAddrWindow(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
void ILI9341_FillRect(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
void ILI9341_FillScreen(uint16_t color);
void ILI9341_DrawPixel(int x, int y, uint16_t color);
void ILI9341_DrawLine(uint16_t color, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void ILI9341_DrawRect(uint16_t color, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void ILI9341_DrawCircle(uint16_t x0, uint16_t y0, int r, uint16_t color);
void ILI9341_SetTextColor(uint16_t color);
void ILI9341_SetBackColor(uint16_t color);
void ILI9341_SetFont(sFONT *pFonts);
void ILI9341_DrawChar(uint16_t x, uint16_t y, uint8_t c);
void ILI9341_String(uint16_t x,uint16_t y, char *str);
void ILI9341_SetRotation(uint8_t r);
void ILI9341_FontsIni(void);
void ILI9341_ini(uint16_t w_size, uint16_t h_size);

#endif
