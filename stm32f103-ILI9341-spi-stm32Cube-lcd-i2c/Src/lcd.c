#include "lcd.h"

extern uint8_t i2c_modul;

uint8_t buf[1]={0};

extern I2C_HandleTypeDef hi2c1;

char str1[100];
uint8_t portlcd; // register for data i2c modul

__STATIC_INLINE void DelayMicro(__IO uint32_t micros)
{
  micros *=(SystemCoreClock / 1000000) / 5;
  while (micros--);
}

void LCD_WriteByteI2CLCD(uint8_t bt)
{
  buf[0]=bt;
  HAL_I2C_Master_Transmit(&hi2c1,(uint16_t) i2c_modul,buf,1,1000);
}

void sendhalfbyte(uint8_t c)
{
  c<<=4;
  e_set();
  DelayMicro(50);
  LCD_WriteByteI2CLCD(portlcd|c);
  e_reset();
  HAL_Delay(2);
}

void sendbyte(uint8_t c, uint8_t mode)
{
  if(mode==0) rs_reset();
  else rs_set();
  uint8_t hc=0;
  hc=c>>4;
  sendhalfbyte(hc);
  sendhalfbyte(c);
  HAL_Delay(2);
}

void LCD_Clear(void)
{
  sendbyte(0x01,0);
}

void LCD_SendChar(char ch)
{
  sendbyte(ch,1);
}

void LCD_String(char* st)
{
  uint8_t i=0;
  while(st[i]!=0)
  {
    sendbyte(st[i],1);
    i++;
  }
}

void LCD_SetPos(uint8_t x, uint8_t y)
{
  switch(y)
  {
    case 0:
    sendbyte(x|0x80,0);
    break;
    case 1:
    sendbyte((0x40+x)|0x80,0);
    break;
    case 2:
    sendbyte((0x14+x)|0x80,0);
    break;
    case 3:
    sendbyte((0x54+x)|0x80,0);
    break;
  }
}

void LCD_ini(void)
{
  HAL_Delay(15);
  sendhalfbyte(0x03);
  sendhalfbyte(0x03);
  sendhalfbyte(0x03);
  sendhalfbyte(0x02);
  sendbyte(0x28,0); // mode two line, 4 bit
  sendbyte(0x0C,0); // display on (D=1), without cursor
  sendbyte(0x01,0); // clean
  sendbyte(0x06,0); // write right
  led1;// led on
  rw0;// write mode
}
