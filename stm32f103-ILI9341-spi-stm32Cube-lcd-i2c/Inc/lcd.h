#ifndef LCD_H_
#define LCD_H_
//------------------------------------------------
#include "stm32f1xx_hal.h"
//------------------------------------------------
#define rs1 LCD_WriteByteI2CLCD(portlcd|=0x01) //установка линии RS в 1
#define rs0 LCD_WriteByteI2CLCD(portlcd&=~0x01) //установка линии RS в 0
#define rs_set() LCD_WriteByteI2CLCD(portlcd|=0x01) //установка линии RS в 1
#define rs_reset() LCD_WriteByteI2CLCD(portlcd&=~0x01) //установка линии RS в 0

#define rw1 LCD_WriteByteI2CLCD(portlcd|=0x02) //установка линии RW в 1
#define rw0 LCD_WriteByteI2CLCD(portlcd&=~0x02) //установка линии RW в 0
#define rw_set() LCD_WriteByteI2CLCD(portlcd|=0x02) //установка линии RW в 1
#define rw_reset() LCD_WriteByteI2CLCD(portlcd&=~0x02) //установка линии RW в 0

#define e1 LCD_WriteByteI2CLCD(portlcd|=0x04)  //установка линии Е в 1
#define e0 LCD_WriteByteI2CLCD(portlcd&=~0x04) //установка линии Е в 0
#define e_set() LCD_WriteByteI2CLCD(portlcd|=0x04)  //установка линии Е в 1
#define e_reset() LCD_WriteByteI2CLCD(portlcd&=~0x04) //установка линии Е в 0

#define led1 LCD_WriteByteI2CLCD(portlcd|=0x08) //установка линии led в 1
#define led0 LCD_WriteByteI2CLCD(portlcd&=~0x08) //установка линии led в 1
#define led_on() LCD_WriteByteI2CLCD(portlcd|=0x08) //установка линии led в 1
#define led_off() LCD_WriteByteI2CLCD(portlcd&=~0x08) //установка линии led в 1
//------------------------------------------------
void LCD_ini(void);
void LCD_Clear(void);
void LCD_SendChar(char ch);
void LCD_String(char* st);
void LCD_SetPos(uint8_t x, uint8_t y);
//------------------------------------------------
#endif /* LCD_H_ */
