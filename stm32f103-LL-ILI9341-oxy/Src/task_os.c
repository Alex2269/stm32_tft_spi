#include "stdlib.h"
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "ILI9341.h"
#include "adc_cfg.h"
#include "task_os.h"

#include "soft_lcd.h"
#include "soft_adc.h"
#include "soft_ina219.h"
#include "soft_pcf8591.h"

#include "pins_ext.h"

#include "time.h"
#include "pid.h"

struct pid_controller_t controller;
struct pid_controller_t pid_1;

// --
float U_power = 12.60; // power voltage
float resistance;
// --
float current_shunt_1;
float shunt_1 = 0.10; // resistance shunt_1
float correction_1 = +0.0;
#define R1 170.0 // resistor feedback to ground
#define R2 345.0 // resistor feedback
#define DELTA_GAIN_1 100.0 // calibration of an error
float gain_amplifier_1 = R2 / R1 * DELTA_GAIN_1;
// --
float current_shunt_2;
float shunt_2 = 63.0;
float correction_2 = +0.0;
#define R3 23000.0 // resistor feedback to ground
#define R4 47000.0 // resistor feedback
#define DELTA_GAIN_2 1.0 // calibration of an error
float gain_amplifier_2 = R4 / R3 * DELTA_GAIN_2;

int16_t nernst;
uint16_t virtuall_gnd;

extern lcd_t *lcd; // pointer to i2c object, for lcd module
extern adc_t *adc; // pointer to i2c object, for ads1115
extern ina_t *ina; // pointer to i2c object, for ina219
// extern pcf8591_t *pcf8591; // pointer to i2c object, for pcf8591

void show_ina219(lcd_t *lcd, ina_t *ina);

float adc_val_0, adc_val_1, adc_val_2, adc_val_3, adc_val_4, adc_val_5, adc_val_6, adc_temp, adc_vref;
float h_cycle=0;

/**
 * it is clocked from system quanta of rtos
 * width base quants_base for example is 100 quanta
 */
void soft_pwm(GPIO_TypeDef *GPIOx, uint32_t Pin, uint16_t duty_cycle, uint16_t quants_base)
{
  osDelay(duty_cycle);
  LL_GPIO_SetOutputPin(GPIOx, Pin);

  osDelay(quants_base-duty_cycle);
  LL_GPIO_ResetOutputPin(GPIOx, Pin);
}

/**
 * it is clocked from system quanta of rtos
 * width base quants_base for example is 100 quanta
 */
void soft_pwm_heating(GPIO_TypeDef *GPIOx, uint32_t Pin, uint16_t duty_cycle, uint16_t quants_base)
{
  LL_GPIO_SetOutputPin(GPIOx, Pin); // enable heating pin

  osDelay(2); // stabilization current heating

  /*
   * Measured resistance heater
   */
  adc_val_6 = read_adc(ADC1, 6);
  current_shunt_1 = ((float)adc_val_6 / gain_amplifier_1) / shunt_1 ; // Current through shunt 1
  resistance = U_power / current_shunt_1 + correction_1; // resistance heating
  if(resistance>99.0) resistance=99.0;
  /*
   * if exit from range
   * protection heating
   * setting the low level on gate mosfet
   */
  if( (resistance < 0.90) || (resistance > 40.5 ))
  {
    LL_GPIO_ResetOutputPin(GPIOx, Pin);
    h_cycle = 0;
  }
  osDelay(duty_cycle);

  LL_GPIO_ResetOutputPin(GPIOx, Pin); // disable heating pin
  osDelay(quants_base-duty_cycle);

  /*
   * The rest is measured when the heater is turned off
   */
  adc_val_0 = read_adc(ADC1, 0);
  adc_val_1 = read_adc(ADC1, 1);
  adc_val_2 = read_adc(ADC1, 2);
  adc_val_3 = read_adc(ADC1, 3);
  adc_val_4 = read_adc(ADC1, 4);
  adc_val_5 = read_adc(ADC1, 5);
  adc_val_6 = read_adc(ADC1, 6);
}

void DefaultTask(void const * argument)
{
  /* BEGIN StartDefaultTask */

  volatile uint16_t pumping=128;
  LL_TIM_OC_SetCompareCH4(TIM3, 128); // pin PB1 virtual ground 2.5 v
  /* Infinite loop */
  for(;;)
  {

    // if(h_cycle > 56) h_cycle = 56;
    if(pumping < 30) pumping = 30;
    if(pumping > 220) pumping = 220;

    if(nernst>450) pumping += 2;
    if(nernst<450) pumping -= 2;

    // soft_pwm_heating(STATUS_LED_GPIO_Port, STATUS_LED_Pin, h_cycle, 200);
    soft_pwm_heating(STATUS_LED_GPIO_Port, STATUS_LED_Pin, pid_get_output(&pid_1), 200);
    // h_cycle += 0.1; // heating cycle duration

    // pcf8591_dac_set(pcf8591, pumping);
    LL_TIM_OC_SetCompareCH3(TIM3, pumping); // pin PB0 pump current

    // LL_GPIO_SetOutputPin(STATUS_LED_GPIO_Port, STATUS_LED_Pin);

    osDelay(5);
  }
  /* END StartDefaultTask */
}

void Task02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */

  // INIT PID_CONTROLLER
  static volatile float input = 0.0f;
  pid_init_controller(&pid_1);
  pid_set_current_time(&pid_1, get_time_ms());
  pid_set_sample_time(&pid_1, 1); // update every 10 msec
  pid_set_tunings(&pid_1, 0.1f, 0.3f, 0.1f);
  pid_set_output_limits(&pid_1, 0.0f, 200.0f);
  pid_set_setpoint(&pid_1, 2.25); // задаем желаемое значение resistance
  // END INIT PID_CONTROLLER

  /* Infinite loop */
  for(;;)
  {
    /* USER CODE BEGIN StartTask02 */
    input = resistance;
    pid_compute(&pid_1, get_time_ms(), input); // вычисление коэфициентов
    /* USER CODE END StartTask02 */
    osDelay(1);
  }
}

void Task03(void const * argument)
{
  /* USER CODE init StartTask03 */

  // INIT PID_CONTROLLER
  pid_init_controller(&controller);
  pid_set_current_time(&controller, get_time_ms());
  pid_set_sample_time(&controller, 10); // update every 10 msec
  pid_set_tunings(&controller, 0.1f, 0.3f, 0.1f);
  pid_set_output_limits(&controller, -4096.0f, 4096.0f);
  pid_set_setpoint(&controller,450.0f);
  // END INIT PID_CONTROLLER
  static volatile float input = 0.0f;

  /* Infinite loop */
  for(;;)
  {
    /* USER CODE BEGIN StartTask03 */
    pid_set_setpoint(&controller,nernst+450.0f);
    input = pid_get_output(&controller);
    pid_compute(&controller, get_time_ms(), input); // вычисление коэфициентов
    /* USER CODE END StartTask03 */
    osDelay(10);
  }
}

// extern float pcf8591_value_in[4];

void Task04(void const * argument)
{
  virtuall_gnd = adc_val_4;

  nernst = abs(adc_val_5-virtuall_gnd); // nernst voltage

  current_shunt_2 = adc_val_3-adc_val_2;
  current_shunt_2 = current_shunt_2 / gain_amplifier_2 / shunt_2; // Current through shunt 2

  ILI9341_setRotation(0);
  ILI9341_setTextSize(2);
  ILI9341_setTextColor(ILI9341_CYAN);
  ILI9341_setCursor(64,   32); ILI9341_draw_fvalue(get_time_sec(),0);
  ILI9341_setCursor(64,   64); ILI9341_draw_value((controller.output)); // pid
  ILI9341_setCursor(64,   96); ILI9341_draw_fvalue((pid_1.output)*1.0f,2);

  ILI9341_setRotation(3);
  ILI9341_setTextSize(5);
  ILI9341_setTextColor(ILI9341_RED);
  ILI9341_setCursor(12,   20); ILI9341_draw_fvalue(current_shunt_2,0);
  ILI9341_setTextColor(ILI9341_YELLOW);
  ILI9341_setCursor(12,   72); (resistance < 10.0 ) ? ILI9341_draw_fvalue(resistance, 2) :  ILI9341_draw_fvalue(resistance, 1);
  ILI9341_setTextColor(ILI9341_BLUE);
  ILI9341_setCursor(12,   124); ILI9341_draw_value(nernst);
  ILI9341_setTextColor(ILI9341_GREEN);

  osDelay(50);
  /* USER CODE END StartTask04 */
}
